package OOPsExplaination;

public abstract class InternationalRelocation {

	// abstract methods
	public abstract void visa();
	
	public abstract void passport();
	
	//non abstract method
	public void relocate () {
		System.out.println("Follow all rule of PR applications");
	}
}
