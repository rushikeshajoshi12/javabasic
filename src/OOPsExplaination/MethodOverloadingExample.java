package OOPsExplaination;

public class MethodOverloadingExample {

	public void moveSpeed(String Car) {
		System.out.println("slow");
	}

	public void moveSpeed(double Car) {
		System.out.println("fast");
	}

	public void moveSpeed(int Train) {
		System.out.println("slow");
	}

	public void moveSpeed(int Train, String airoplane) {

		System.out.println("fast");
	}

	public static void main(String[] args) {
		MethodOverloadingExample overLoad = new MethodOverloadingExample();
		overLoad.moveSpeed("Kia");
		overLoad.moveSpeed(80);

	}

}
