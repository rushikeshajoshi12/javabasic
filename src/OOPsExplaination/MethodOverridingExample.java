package OOPsExplaination;

public class MethodOverridingExample extends Classexplaination {
	// applyBrake method of class Classexplanation is overridden in this class
	@Override
	 public void applyBrake () {
	System.out.println("Car stopped after brakes applied");
	}

	public static void main(String[] args) {
		MethodOverridingExample override = new MethodOverridingExample ();
		override.applyBrake ();
		override.company="Toyota";
		override.model="Camery";

	}

}
