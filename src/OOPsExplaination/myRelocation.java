package OOPsExplaination;

public class myRelocation extends InternationalRelocation {

	@Override
	public void visa() {
		System.out.println("Visa not needed for relocation");
		
	}

	@Override
	public void passport() {
		System.out.println("Passport with 1 year validity needed");
	}
		public static void main(String[] args) {
			InternationalRelocation relocation = new myRelocation();
			relocation.visa();
			relocation.passport();
			relocation.relocate();

	}			
}

