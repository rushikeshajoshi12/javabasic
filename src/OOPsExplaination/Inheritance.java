package OOPsExplaination;

public class Inheritance extends Classexplaination {
	
	public void accelerate () {
		System.out.println("car of : "  +company+  " and model: " +model+  " is able to accelerate");
	}

	public static void main(String[] args) {
		Inheritance accCar= new Inheritance();
		// attributes inherited from superclas Classexplaination
		accCar.company="Maruti";
		accCar.model="Swiftdesire";
		// methods inherited from superclass name Classexplaination
		accCar.startEngene();
		accCar.applyBrake();
		// methods in this present subclass used
		accCar.accelerate();		

	}

}
