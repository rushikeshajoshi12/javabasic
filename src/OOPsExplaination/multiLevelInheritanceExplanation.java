package OOPsExplaination;

public class multiLevelInheritanceExplanation extends Inheritance {

	// Attribute created in this Sub Class
	String gearBoxType;
	
	// Method created in this Sub Class
	public void gearBox() {
	System.out.println("Car of: " +company+  " and model : " +model+ " has " +gearBoxType+ " gear box");
}
	
	public static void main(String[] args) {
		multiLevelInheritanceExplanation gearOfCar = new multiLevelInheritanceExplanation ();
		// Attributes inherited from Super Class named Classexplanation
		gearOfCar.company="Maruti";
		gearOfCar.model = "Swiftdesire";
		// Methods inherited from Super Class named Classexplanation
		gearOfCar.startEngene();
		gearOfCar.applyBrake();
		// Methods inherited from Super Class named Inheritance
		gearOfCar.accelerate();
		// Attribute created in this Sub Class is used
		gearOfCar.gearBoxType = "Manualhandling";
		// Method created in this Sub Class is used
		gearOfCar.gearBox();
		

	}

}
