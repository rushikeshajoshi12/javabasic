package JavaBasic;

public class StringExample {

	public static void main(String[] args) {

		// String str = new String("mobile:" + "android");
		// creating Java string by new keyword
		// this statement create two object i.e
		// first the object is created in heap
		// memory area and second the object is
		// created in String constant pool.
		// System.out.println(str);
		// String demoString = new String ("give respect take respect");
		// System.out.println(demoString);
		String str = "Java programing language";
		System.out.println(str + " is a computer Programing language");
		// Get the character at positions 0 and 10.
		int index1 = str.charAt(0);
		int index2 = str.charAt(14);
		// Print out the results.
		System.out.println("The charector at position 0 is " + (char) index1);
		System.out.println("The charector at position 0 is " + (char) index2);
	}

}
