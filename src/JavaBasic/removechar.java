package JavaBasic;

public class removechar {

	public static void main(String[] args) {
		String str = "this is java method";
		System.out.println(removeCharAt(str, 6));
	}

	public static String removeCharAt(String s, int pos) {
		return s.substring(0, pos) + s.substring(pos + 1);
	}

}
