package JavaBasic;

public class StringReverseExample {

	public static void main(String[] args) {
		String str = "Rushi";
		String reverse = new StringBuffer(str).reverse().toString();
		System.out.println("String before reverse: " + str);
		System.out.println("String after reverse: " + reverse);

	}

}
