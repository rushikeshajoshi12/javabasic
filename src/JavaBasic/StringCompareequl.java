package JavaBasic;

public class StringCompareequl {

	public static void main(String[] args) {
		//String compare by equals()
		String s1 = "Rushikesh";
		String s2 = "Rushikesh";
		String s3 = new String("rushikesh");
		System.out.println(s1.equals(s2));
		System.out.println(s2.equals(s3));

		// String compare by == operator
		String a1 = "Bhargavi";
		String a2 = "Bhargavi";
		String a3 = new String("bhargavi");
		System.out.println(a1 == a2);
		System.out.println(a2 == a3);
	}

}
