package JavaBasic;

public class StringToUpperCaseExample {

	public static void main(String[] args) {
		String str = "rushikesh";
		String str1 = "BHARGAVI";
		String strUpper = str.toUpperCase();
		String strlower = str1.toLowerCase();
		System.out.println("String1: " + str+ " & String2: " +str1);
		System.out.println("String1 changed to upper case: " + strUpper);
		System.out.println("String2 changed to lower case : " + strlower);
	}

}
