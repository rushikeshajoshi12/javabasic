package Collection;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListexample {

	public static void main(String[] args) {
		LinkedList<String> link = new LinkedList();
		link.add("Bhargavi");
		link.add("Rushi");
		link.add("Krishnai");
		link.add("Nandini");
		link.remove(1);
		System.out.println(link);
		link.addFirst("Chinnu");
		link.addLast("Bittu");
		link.getFirst();
		link.getLast();
		System.out.println("After adding first and last element:" + link);
		Iterator<String> itr = link.iterator();
		/*
		 * while (itr.hasNext()) { System.out.println(itr.next());
		 */
		LinkedList<String> link2 = new LinkedList();
		link2.add("Mandar");
		link2.add("Purushottam");
		link2.add("Yogeshwari");
		link2.addFirst("Rushikesh");
		link2.addLast("Anita");
		System.out.println(link2);
		link.addAll(link2);
		System.out.println("After adding link2 in link" + link);
		LinkedList link3=new LinkedList<>();
		link3.add(null);
		link3.add("Ram");
		link3.add(10);
		link3.addLast("Raja");
		System.out.println(link3);
		}

}
