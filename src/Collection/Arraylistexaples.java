package Collection;

import java.util.ArrayList;

public class Arraylistexaples {

	public static void main(String[] args) {

		// create Integer type arraylist
		/*
		 * ArrayList<Integer> ial = new ArrayList<Integer>();
		 *  ial.add(3);
		 *   ial.add(40);
		 * System.out.println("check the numbers present in the list:" + ial);
		 */

		// create String type arraylist

		ArrayList<String> languages = new ArrayList<>();
		languages.add("Marathi");
		languages.add("Sannskrit");
		languages.add("Kannada");
		languages.add("Hindi");
		languages.add("English");
		languages.add("Gujrati");
		//System.out.println("How many languages I can understand:" + languages);

		 // get the element from the arraylist
		//String str=languages.get(3);
		//System.out.println("element present at index3 is :"+str);
		
		 // change the element of the array list
		languages.set(1, "Sanskrut");
		System.out.println("After change the arraylist:"+languages);
		 
		// remove element from index 2
	    String rmv = languages.remove(5);
	    System.out.println("Updated ArrayList: " + languages);
	    System.out.println("Removed Element: " + rmv);
	    //check the size
	   int count= languages.size();
	   System.out.println("size of array is "+count);
	   boolean show=languages.isEmpty();
	   System.out.println("check"+show);
	  //iterate using for each loop
	   for (String lng:languages) {
		   System.out.println(lng);
		   System.out.println("::::::::::::");
      
	 
	   }
	   
	   }

}
