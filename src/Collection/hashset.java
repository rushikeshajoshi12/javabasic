package Collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class hashset {

	public static void main(String[] args) {
		// Creating HashSet and adding elements
		HashSet<String> set = new HashSet<String>();
		set.add("Ravi");
		set.add("Vijay");
		set.add("Ravi");
		set.add("Ajay");
		set.add("Rushi");
		System.out.println(set);//insersion order not preserved
		LinkedHashSet<String> lset = new LinkedHashSet<String>();
		lset.add("Rushi");
		lset.add("mandar");
		lset.add("Puru");
		lset.add("Yogi");		
		System.out.println(lset);//insersion order is preserved
		TreeSet<String> tset=new TreeSet<String>();
		tset.add("bhargavi");
		tset.add("anita");
		tset.add("krishnai");
		System.out.println(tset);

	}

}
